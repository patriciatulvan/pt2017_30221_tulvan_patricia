package polynomial;

public class Monom implements Comparable<Monom> {
	private Double coeficient;
	private Integer exponent;
	
	public Monom(){
	
	}
	
	public Monom(Double coeficient,Integer exponent){
		this.coeficient=coeficient;
		this.exponent=exponent;
	}
	
	public void setCoeficient(Double coeficient){
		this.coeficient=coeficient;
	}
	
	public void setExponent(Integer exponent) {
		this.exponent = exponent;
	}
	
	public Double getCoeficient(){
		return coeficient;
	}
	
	public Integer getExponent(){
		return exponent;
	}

	//pentru a putea sorta polinoamele in functie de exponent
	@Override
	public int compareTo(Monom m) {
		return m.exponent.compareTo(exponent);
	}
}
