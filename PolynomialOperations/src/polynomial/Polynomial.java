package polynomial;
import java.util.ArrayList;
import java.util.Collections;

public class Polynomial{
	private ArrayList<Monom> poly;
	
	public Polynomial(){
		poly = new ArrayList<Monom>();
	}
	
	public Polynomial(ArrayList<Monom> poly){
		this.poly=poly;
	}
	
	public Polynomial add(Polynomial p){
		ArrayList<Monom> b= new ArrayList<Monom>();
		ArrayList<Monom> a = new ArrayList<Monom>();
		ArrayList<Monom> result = new ArrayList<Monom>();
		ArrayList<Monom> aux = new ArrayList<Monom>();
		
		a.addAll(p.getPoly());
		
		if(poly.size()>=a.size())
			b.addAll(poly);
		else{
			b.addAll(a);
			a.clear();
			a.addAll(poly);
		}
		
		for(Monom i: b)
			for(Monom j: a)
				if(i.getExponent() == j.getExponent())
					result.add(new Monom(i.getCoeficient()+j.getCoeficient(),i.getExponent()));
		
		aux.addAll(result);
		
		for(Monom j: a){
			int ok=1;
			for(Monom i: aux)
				if(j.getExponent()==i.getExponent())
					ok=0;
			if(ok==1)
				result.add(j);
		}
		
		aux.clear();
		aux.addAll(result);
		
		for(Monom j: b){
			int ok=1;
			for(Monom i: aux)
				if(j.getExponent()==i.getExponent())
					ok=0;
			if(ok==1)
				result.add(j);
		}
		
		aux.clear();
		aux.addAll(result);
		
		for(Monom i: aux)
			if(i.getCoeficient().intValue()==0)
				result.remove(i);
		
		return new Polynomial(result);
	}
	
	public Polynomial subtract(Polynomial p){
		ArrayList<Monom> b= new ArrayList<Monom>();
		ArrayList<Monom> a = new ArrayList<Monom>();
		ArrayList<Monom> result = new ArrayList<Monom>();
		ArrayList<Monom> aux = new ArrayList<Monom>();
		
		a.addAll(p.getPoly());
		
		if(poly.size()>=a.size())
			b.addAll(poly);
		else{
			b.addAll(a);
			a.clear();
			a.addAll(poly);
		}
		
		for(Monom index: a)
			index.setCoeficient(-index.getCoeficient());
			
		
		for(Monom i: b)
			for(Monom j: a)
				if(i.getExponent() == j.getExponent())
					result.add(new Monom((i.getCoeficient()+j.getCoeficient()),i.getExponent()));
			
		aux.addAll(result);
	
		for(Monom j: a){
			int ok=1;
			for(Monom i: aux)
				if(j.getExponent()==i.getExponent())
					ok=0;
			if(ok==1)
				result.add(j);
		}
		
		aux.clear();
		aux.addAll(result);
		
		for(Monom j: b){
			int ok=1;
			for(Monom i: aux)
				if(j.getExponent()==i.getExponent())
					ok=0;
			if(ok==1)
				result.add(j);
		}
		
		aux.clear();
		aux.addAll(result);
		
		for(Monom i: aux)
			if(i.getCoeficient().intValue()==0)
				result.remove(i);
		
		return new Polynomial(result);
	}
	
	public Polynomial derive(){
		ArrayList<Monom> a= new ArrayList<Monom>();
		ArrayList<Monom> aux = new ArrayList<Monom>();
		for(Monom index: poly){
			Double c;
			Integer e;
			if (index.getExponent()==0){
				c=0.0;
				e=0;
			}
			else{
				c= index.getCoeficient()*index.getExponent();
				e= index.getExponent()-1;
			}
			a.add(new Monom(c,e));
		}
		aux.addAll(a);
		for(Monom i: aux)
			if(i.getCoeficient()==0)
				a.remove(i);
		
		return new Polynomial(a);
	}
	
	public Polynomial integrate(){
		ArrayList<Monom> a= new ArrayList<Monom>();
		for(Monom index: poly){
			Double c;
			Integer e;
			c= index.getCoeficient()/(index.getExponent()+1);
			e= index.getExponent()+1;
			a.add(new Monom(c,e));
		}
		
		return new Polynomial(a);
	}
	
	public Polynomial multiply(Polynomial p){
		if(p==null)
			return new Polynomial(poly);
		
		ArrayList<Monom> b= new ArrayList<Monom>();
		ArrayList<Monom> a= new ArrayList<Monom>();
		ArrayList<Monom> d= new ArrayList<Monom>();
		a.addAll(p.getPoly());
		
		if(poly.size()>=a.size())
			b.addAll(poly);
		else{
			b.addAll(a);
			a.clear();
			a.addAll(poly);
		}
		for(Monom i: a)
			for(Monom j: b){
				Double c;
				Integer e;
				c=i.getCoeficient()*j.getCoeficient();
				e=i.getExponent()+j.getExponent();
				d.add(new Monom(c,e));
			}
		
		Polynomial r= new Polynomial(d);
		r.sort();
		
		return r;
	}
	
	// metoda folosita doar in cadrul metodei de impartire a polinoamelor
	private Polynomial devidePoly(Monom p,Monom q){
		Monom aux = new Monom();
		aux.setCoeficient(p.getCoeficient()/q.getCoeficient());
		aux.setExponent(p.getExponent()-q.getExponent());
		
		ArrayList<Monom> a= new ArrayList<Monom>();
		a.add(aux);
		
		return new Polynomial(a);
	}
	
	public Polynomial devide(Polynomial p){
		Polynomial cm = new Polynomial();
		Polynomial c = new Polynomial();
		Polynomial b=this;
		while(b.getGrade()>= p.getGrade()){
				cm=devidePoly(b.getMaxExp(), p.getMaxExp());
				Polynomial aux=p.multiply(cm);
				Polynomial d=c.add(cm);
				c=d;
				Polynomial e=b.subtract(aux);
				b=e;
		}
		
		return c;
			
	}
	
	// returneaza monomul de grad maxim
	public Monom getMaxExp(){
		Integer i= this.getGrade();
		Monom max = new Monom();
		
		for(Monom index: poly)
			if(index.getExponent()==i){
				max=index;
			}
		
		return max;
	}
	
	// transforma polinomul in String pentru a putea fi afisat in formatul corespunzator
	public String printPolynomial(){
		sort();
		String out = null;
		for(Monom index: poly){
			if(out!=null){
				if(index.getExponent()!= 0)
					out=out + index.getCoeficient()+"x^"+index.getExponent()+"+";
				else
					out=out + index.getCoeficient()+ "+";
			}
			else{
				if(index.getExponent()!= 0)
					out=index.getCoeficient()+"x^"+index.getExponent()+"+";
				else
					out=index.getCoeficient()+ "+";
			}
		}
		
		out=out.substring(0, out.length()-1);
		return out;
		
	}
	
	public ArrayList<Monom> getPoly(){
		return poly;
	}
	
	public Integer getGrade(){
		Integer max=0;
		for(Monom index: poly)
			if(index.getExponent()>max)
				max=index.getExponent();
		return max;
	}
	
	// realizeaza sortarea elementelor polinomului in functie de exponent
	public void sort(){
		Collections.sort(poly);
	}
}
